import React from 'react';
import './Countries.css';

const Countries = ({onClick, countryName}) => {
    return (
        <li onClick={onClick}>{countryName}</li>
    );
};

export default Countries;