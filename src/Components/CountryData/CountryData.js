import React, {useEffect, useState} from 'react';
import './CountryData.css';
import axios from "axios";
import {COUNTRY_CODE_URL} from "../../config";

const CountryData = ({countryCode}) => {
    const [countryInfo, setCountryInfo] = useState(null);
    const [borders, setBorders] = useState([]);

    useEffect(() => {

        const fetchData = async () => {
            if (countryCode === null) return;

            const response = await axios.get(COUNTRY_CODE_URL + '/' + countryCode);
            setCountryInfo(response.data);
        };

        fetchData().catch(console.error);

    }, [countryCode]);

    useEffect(() => {
        const fetchData = async () => {
            if (countryCode === null) return;

            const response = await axios.get(COUNTRY_CODE_URL + '/' + countryCode);
            const countryBorders = response.data.borders;
            const borderPromises = await Promise.all(countryBorders.map(async countryCode => {
                const response2 = await axios.get(COUNTRY_CODE_URL + '/' + countryCode);
                return response2.data.name;
            }));

            setBorders(borderPromises);
        };

        fetchData().catch(console.error);
    }, [countryCode]);

    return countryInfo && (
        <div className="CountryInfo"
        >

            <h2>{countryInfo.name}</h2>
            <h4>Capital: {countryInfo.capital}</h4>
            <p><strong>Flag:</strong></p>
            <img src={countryInfo.flag} width="200px" height="auto" alt="flag"/>
            <p><strong>Population: </strong>{countryInfo.population}</p>
            <strong>Borders with: </strong>
            {borders.length ? <ul>
                {borders.map((country) =>
                    <li key={country}>{country}</li>
                )}
            </ul> : <p>the country has no borders</p>}



        </div>
    );
};

export default CountryData;