import React, {useEffect, useState} from 'react';
import './MainPage.css';
import Countries from "../../Components/Countries/Countries";
import axios from "axios";
import {COUNTRIES_URL} from "../../config";
import CountryData from "../../Components/CountryData/CountryData";

const MainPage = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axios.get(COUNTRIES_URL);
            const countries = response.data;
            setCountries(countries);

            const countriesCodes = [];

            countries.forEach(country => {
                if (!countriesCodes.includes(country.alpha3Code)) {
                    countriesCodes.push(country.alpha3Code);
                }
            });


        };
        fetchData().catch(e => console.error(e));
    }, []);

    return (
        <div className="MainPage">
            <div className="Countries">
                <ul className='CountriesList'>
                    {countries.map(country => (
                        <Countries
                            key={country.alpha3Code}
                            countryName={country.name}
                            onClick={() => setSelectedCountry(country.alpha3Code)}
                        />
                    ))}
                </ul>


            </div>
            <div className="CountryInfoBlock">
                {selectedCountry ? <CountryData countryCode={selectedCountry}/> : <h2>Please select country</h2>}
            </div>
        </div>

    );
};

export default MainPage;