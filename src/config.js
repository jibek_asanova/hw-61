export const BASE_URL = 'https://restcountries.eu/rest/v2/';
export const COUNTRIES_URL = BASE_URL + 'all?fields=name;alpha3Code';
export const COUNTRY_CODE_URL = BASE_URL + 'alpha';
export const COUNTRY_ALL_URL ='https://restcountries.eu/rest/v2/all';
